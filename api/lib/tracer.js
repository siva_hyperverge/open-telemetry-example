const opentelemetry = require('@opentelemetry/api');
const { NodeTracerProvider } = require("@opentelemetry/node");
const { SimpleSpanProcessor,ConsoleSpanExporter,BatchSpanProcessor } = require("@opentelemetry/tracing");
const { CollectorTraceExporter } = require('@opentelemetry/exporter-collector');
const EXPORTER = process.env.EXPORTER || 'console';
const fs = require('fs');


module.exports = (serviceName) => {
    const tracerProvider = new NodeTracerProvider({
        plugins: {
          express: {
            enabled: true,
            path: '@opentelemetry/plugin-express',
          },
          http: {
            enabled: true,
            path: '@opentelemetry/plugin-http',
          },
        },
    });

    const address = 'http://collector:55681/v1/trace'
    const exporterCollector = new CollectorTraceExporter({
	      serviceName,
	      url: address,
    });
    tracerProvider.addSpanProcessor(new SimpleSpanProcessor(exporterCollector));

    tracerProvider.register();

    return opentelemetry.trace.getTracer(serviceName);
}

