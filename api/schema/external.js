const Joi = require('@hapi/joi');
const schema = Joi.object({
  apis: Joi.array().items(Joi.string().uri({ scheme: ['https', "http"]})),
})
module.exports = schema
