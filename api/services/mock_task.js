
const delay = ms => new Promise(res => setTimeout(res, ms));

class MockTask {

   async perform(){
   await delay(500)
   }
}

module.exports = {
 MockTask
}
