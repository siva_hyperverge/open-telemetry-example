const axios = require('axios');

class Client {
 async process(url){
     try{
     	const response = await axios.get(url);
     	return response.data
     }catch(error){
        return "Failed"
     }
 }

}

module.exports ={
 Client
}
