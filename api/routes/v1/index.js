const  express = require("express");
const router = express.Router();
const {task, error} = require("../../controllers/mock")
const  asyncHandler = require("../../middleware/async")
const {externalOperations} = require("../../controllers/external")
router
  .get('/internal', asyncHandler(task));

router
  .route('/external')
  .post(asyncHandler(externalOperations));

router
  .route('/500')
  .get(asyncHandler(error));

router.route('/').get( function (req, res) {
      res.send("Aok");
});


module.exports = router;
