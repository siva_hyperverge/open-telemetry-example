const schema = require("../schema/external")
const {Client} = require("../services/client")

const client = new Client()

const externalOperations = async function (req, res){
    console.log("externalOperations")
    let value = undefined 
    try {
      value = await schema.validateAsync(req.body);
    } catch (error) {
       res.status(400).send(error.details)
    }

        const unresolvedPromises = value.apis.map(api =>  client.process(api));	

    const results = await Promise.all(unresolvedPromises);
    res.status(200).send(results)
}

module.exports = {
   externalOperations
}

