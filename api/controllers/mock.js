const {MockTask} = require("../services/mock_task")
const {MockError} = require("../services/mock_error")
const asyncHandler = require( '../middleware/async');

const mockTask = new MockTask()
const mockError = new MockError()

const task = async function (req, res){
   await mockTask.perform()
   res.status(200).send({"mesasge":"ok"})
}


const error = async function(req, res){
  await mockError.perform()
}

module.exports= {
   task,
   error
}

