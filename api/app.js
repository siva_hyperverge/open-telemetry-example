const tracer = require('./lib/tracer')(process.env.SERVER_NAME);

const express = require('express');
const api = require("@opentelemetry/api");
const bodyParser = require('body-parser');

const routes = require("./routes");

var app = express();


app.set('port', process.env.PORT);
console.log(process.env.PORT)
app.use(bodyParser.json({limit: 1024}));
app.use(bodyParser.urlencoded({limit: 1024, extended: true}));

// Reference: https://stackoverflow.com/questions/13554319/express-js-close-response
app.use(function(req, res, next) {
    res.setHeader('Connection', 'close');
    next();
});


app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


app.use(function(err, req, res, next) {
    const span = tracer.startSpan("Error", { parent: tracer.getCurrentSpan() });
    span.setStatus({

      // use an appropriate status code here
      code: api.StatusCode.ERROR,
      message: err.message,
    });
    span.end()
    
    // console.log(err)
        return next()
});
app.listen(process.env.PORT, ()=>{
    console.log("listen")
})
module.exports = app;
